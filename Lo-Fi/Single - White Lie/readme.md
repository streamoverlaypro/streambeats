# White Lie

## by StreamBeats by Harris Heller

<img src="artwork.jpg" width="300" />

| Track | Title     | Author                       | length |
| ----- | --------- | ---------------------------- | ------ |
| 1     | White Lie | StreamBeats by Harris Heller | 2:38   |

[Back to List of albums](/Lo-Fi/)
