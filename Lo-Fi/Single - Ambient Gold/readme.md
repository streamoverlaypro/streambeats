# Ambient Gold

## by StreamBeats by Harris Heller

<img src="artwork.jpg" width="300" />

| Track | Title        | Author                       | length |
| ----- | ------------ | ---------------------------- | ------ |
| 1     | Ambient Gold | StreamBeats by Harris Heller | 2:43   |

[Back to List of albums](/Lo-Fi/)
