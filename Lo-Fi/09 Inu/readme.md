# 9. Inu

## StreamBeats by Harris Heller

<img src="artwork.jpg" width="300" />

| Track | Title                | Author                       | length |
| ----- | -------------------- | ---------------------------- | ------ |
| 1     | Akita Inu            | StreamBeats by Harris Heller | 2:20   |
| 2     | Stuck in Wonderland  | StreamBeats by Harris Heller | 2:15   |
| 3     | Calm Waters          | StreamBeats by Harris Heller | 2:08   |
| 4     | Trips to the Moon    | StreamBeats by Harris Heller | 2:11   |
| 5     | Warm Snow            | StreamBeats by Harris Heller | 2:05   |
| 6     | Time Keys            | StreamBeats by Harris Heller | 2:13   |
| 7     | Orbis                | StreamBeats by Harris Heller | 2:32   |
| 8     | Softshell            | StreamBeats by Harris Heller | 2:26   |
| 9     | N64                  | StreamBeats by Harris Heller | 2:19   |
| 10    | All the Royalties    | StreamBeats by Harris Heller | 2:13   |
| 11    | Memorized Sunset     | StreamBeats by Harris Heller | 2:22   |
| 12    | Most Requested       | StreamBeats by Harris Heller | 2:15   |
| 13    | Glowing              | StreamBeats by Harris Heller | 2:20   |
| 14    | Remembering Tani     | StreamBeats by Harris Heller | 2:03   |
| 15    | Don't Fall for Me    | StreamBeats by Harris Heller | 2:12   |
| 16    | Mint Frost           | StreamBeats by Harris Heller | 2:16   |
| 17    | Lily's Eyes          | StreamBeats by Harris Heller | 2:03   |
| 18    | Unwanted Peace       | StreamBeats by Harris Heller | 2:04   |
| 19    | Ice on My Tongue     | StreamBeats by Harris Heller | 2:24   |
| 20    | Be My Pegasus        | StreamBeats by Harris Heller | 2:30   |
| 21    | Beach Flavored Sand  | StreamBeats by Harris Heller | 2:18   |
| 22    | Anime and Chill      | StreamBeats by Harris Heller | 2:28   |
| 23    | Let's Call Mom       | StreamBeats by Harris Heller | 2:13   |
| 24    | I Know I Lied        | StreamBeats by Harris Heller | 2:24   |
| 25    | Banana Fuego         | StreamBeats by Harris Heller | 2:05   |
| 26    | When Will Never Come | StreamBeats by Harris Heller | 2:28   |
| 27    | Neptune              | StreamBeats by Harris Heller | 2:32   |
| 28    | Buffering            | StreamBeats by Harris Heller | 2:39   |
| 29    | No Blue Sky          | StreamBeats by Harris Heller | 2:47   |
| 30    | Wish I Was Prime     | StreamBeats by Harris Heller | 2:26   |

[Back to List of albums](/Lo-Fi/)
